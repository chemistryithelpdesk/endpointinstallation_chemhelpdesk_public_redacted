# Endpoint Installation Tool [REDACTED], by UW-Madison Chemistry Dept. IT Helpdesk

This repository contains a series of scripts written by the UW-Madison Department of Chemistry IT Helpdesk to better automate the process of setting up a new chemistry device on **Windows**, **Linux**, and **MacOS**[To be released soon].

This repository is PUBLIC. For the original INTERNAL version, access it here: [endpointinstallation_chemhelpdesk_internal](https://git.doit.wisc.edu/ortizlunyov/endpointinstallation_chemhelpdesk_internal)


This project uses Semantic Versioning, but with slight modifications. This is to keep track of development between different versions of the script.

[_MAJOR Release_]**.**[_Substantial Release_ (up to 9, then MAJOR)]**.**[_Minor Release_/_Release Bug fix_]**.**[_Any Pre-release edits_]**-**[_Development Stage_]